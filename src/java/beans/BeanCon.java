/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import model.Categorias;
import model.Escuderias;
import model.Pilotos;

/**
 *
 * @author Mixtomeister
 */
@Stateless
public class BeanCon{

    @PersistenceUnit
    EntityManagerFactory emf;

    public List getAll(String tabla) {
        return emf.createEntityManager().createNamedQuery(tabla + ".findAll").getResultList();
    }
    
    public void insert(Pilotos item){
        EntityManager em = emf.createEntityManager();
        em.persist(item);
    }
    
    public void insert(Escuderias item){
        EntityManager em = emf.createEntityManager();
        em.persist(item);
    }
    
    public void insert(Categorias item){
        EntityManager em = emf.createEntityManager();
        em.persist(item);
    }
    
    public void update(Pilotos item, int id){
        EntityManager em = emf.createEntityManager();
        Pilotos piloto = em.find(Pilotos.class, id);
        em.merge(piloto);
        piloto.setNombre(item.getNombre());
        piloto.setApellido(item.getApellido());
        if(item.getEscuderia() != null){
            piloto.setEscuderia(em.find(Escuderias.class, item.getEscuderia().getId()));
        }else{
            piloto.setEscuderia(null);
        }
        
    }
    
    public void update(Escuderias item, int id){
        EntityManager em = emf.createEntityManager();
        Escuderias esc = em.find(Escuderias.class, id);
        em.merge(esc);
        esc.setNombreEquipo(item.getNombreEquipo());
        esc.setNombreEquipoCorto(item.getNombreEquipoCorto());
        esc.setCategoria(em.find(Categorias.class, item.getCategoria().getId()));
    }
    
    public void update(Categorias item, int id){
        EntityManager em = emf.createEntityManager();
        Categorias cat = em.find(Categorias.class, id);
        em.merge(cat);
        cat.setCategoria(item.getCategoria());
    }
    
    public void delete(int id, String tabla){
        EntityManager em = emf.createEntityManager();
        if(tabla.equals("Pilotos")){
            Pilotos piloto = em.find(Pilotos.class, id);
            em.remove(piloto);
        }else if(tabla.equals("Escuderias")){
            Escuderias esc = em.find(Escuderias.class, id);
            em.remove(esc);
        }else if(tabla.equals("Categorias")){
            Categorias cat = em.find(Categorias.class, id);
            em.remove(cat);
        }
    }
}
