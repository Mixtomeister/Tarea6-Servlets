/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Mixtomeister
 */
public interface GestionApp {
     void getAll(HttpServletRequest request, HttpServletResponse response);
     void get(HttpServletRequest request, HttpServletResponse response);
     void insert(HttpServletRequest request, HttpServletResponse response);
     void update(HttpServletRequest request, HttpServletResponse response);
     void remove(HttpServletRequest request, HttpServletResponse response);
}
