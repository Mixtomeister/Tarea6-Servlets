/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import beans.BeanCon;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Categorias;
import model.Escuderias;
import model.Pilotos;

/**
 *
 * @author Mixtomeister
 */
@WebServlet(name = "GestionServlet", urlPatterns = {"/GestionServlet"})
public class GestionServlet extends HttpServlet implements GestionApp{
    
    @EJB
    BeanCon bean;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        switch(request.getParameter("pet")){
            case "getAll":
                getAll(request, response);
                break;
            case "get":
                get(request, response);
                break;
            case "insert":
                insert(request, response);
                break;
            case "update":
                update(request, response);
                break;
            case "delete":
                remove(request, response);
                break;
        }
    }

    @Override
    public void getAll(HttpServletRequest request, HttpServletResponse response) {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        try {
            gson.toJson(bean.getAll(request.getParameter("table")), response.getWriter());
        } catch (IOException ex) {
            
        }
    }

    @Override
    public void get(HttpServletRequest request, HttpServletResponse response) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void insert(HttpServletRequest request, HttpServletResponse response) {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        if(request.getParameter("table").equals("Pilotos")){
            Pilotos item = gson.fromJson(request.getParameter("item"), Pilotos.class);
            bean.insert(item);
        }else if(request.getParameter("table").equals("Escuderias")){
            Escuderias item = gson.fromJson(request.getParameter("item"), Escuderias.class);
            bean.insert(item);
        }else if(request.getParameter("table").equals("Categorias")){
            Categorias item = gson.fromJson(request.getParameter("item"), Categorias.class);
            bean.insert(item);
        }
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.print("{\"estado\": \"OK\"}");
        } catch (IOException ex) {
            
        }
    }

    @Override
    public void update(HttpServletRequest request, HttpServletResponse response) {
        Gson gson = new Gson();
        if(request.getParameter("table").equals("Pilotos")){
            System.out.println(request.getParameter("item"));
            Pilotos item = gson.fromJson(request.getParameter("item"), Pilotos.class);
            bean.update(item, Integer.parseInt(request.getParameter("id")));
        }else if(request.getParameter("table").equals("Escuderias")){
            Escuderias item = gson.fromJson(request.getParameter("item"), Escuderias.class);
            bean.update(item, Integer.parseInt(request.getParameter("id")));
        }else if(request.getParameter("table").equals("Categorias")){
            Categorias item = gson.fromJson(request.getParameter("item"), Categorias.class);
            bean.update(item, Integer.parseInt(request.getParameter("id")));
        }
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.print("{\"estado\": \"OK\"}");
        } catch (IOException ex) {
            
        }
    }

    @Override
    public void remove(HttpServletRequest request, HttpServletResponse response) {
        bean.delete(Integer.parseInt(request.getParameter("item")), request.getParameter("table"));
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.print("{\"estado\": \"OK\"}");
        } catch (IOException ex) {
            
        }
    }
    
    
}
