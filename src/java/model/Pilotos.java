/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import com.google.gson.annotations.Expose;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Mixtomeister
 */
@Entity
@Table(name = "pilotos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pilotos.findAll", query = "SELECT p FROM Pilotos p")
    , @NamedQuery(name = "Pilotos.findById", query = "SELECT p FROM Pilotos p WHERE p.id = :id")
    , @NamedQuery(name = "Pilotos.findByNombre", query = "SELECT p FROM Pilotos p WHERE p.nombre = :nombre")
    , @NamedQuery(name = "Pilotos.findByApellido", query = "SELECT p FROM Pilotos p WHERE p.apellido = :apellido")})
public class Pilotos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    @Expose
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "nombre")
    @Expose
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "apellido")
    @Expose
    private String apellido;
    @JoinColumn(name = "escuderia", referencedColumnName = "id")
    @ManyToOne
    @Expose
    private Escuderias escuderia;

    public Pilotos() {
    }

    public Pilotos(Integer id) {
        this.id = id;
    }

    public Pilotos(Integer id, String nombre, String apellido) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Escuderias getEscuderia() {
        return escuderia;
    }

    public void setEscuderia(Escuderias escuderia) {
        this.escuderia = escuderia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pilotos)) {
            return false;
        }
        Pilotos other = (Pilotos) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Pilotos[ id=" + id + " ]";
    }
    
}
