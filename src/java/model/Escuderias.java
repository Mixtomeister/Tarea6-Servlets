/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import com.google.gson.annotations.Expose;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Mixtomeister
 */
@Entity
@Table(name = "escuderias")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Escuderias.findAll", query = "SELECT e FROM Escuderias e")
    , @NamedQuery(name = "Escuderias.findById", query = "SELECT e FROM Escuderias e WHERE e.id = :id")
    , @NamedQuery(name = "Escuderias.findByNombreEquipo", query = "SELECT e FROM Escuderias e WHERE e.nombreEquipo = :nombreEquipo")
    , @NamedQuery(name = "Escuderias.findByNombreEquipoCorto", query = "SELECT e FROM Escuderias e WHERE e.nombreEquipoCorto = :nombreEquipoCorto")})
public class Escuderias implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    @Expose
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "nombre_equipo")
    @Expose
    private String nombreEquipo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "nombre_equipo_corto")
    @Expose
    private String nombreEquipoCorto;
    @OneToMany(mappedBy = "escuderia")
    private Collection<Pilotos> pilotosCollection;
    @JoinColumn(name = "categoria", referencedColumnName = "id")
    @ManyToOne(optional = false)
    @Expose
    private Categorias categoria;

    public Escuderias() {
    }

    public Escuderias(Integer id) {
        this.id = id;
    }

    public Escuderias(Integer id, String nombreEquipo, String nombreEquipoCorto) {
        this.id = id;
        this.nombreEquipo = nombreEquipo;
        this.nombreEquipoCorto = nombreEquipoCorto;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombreEquipo() {
        return nombreEquipo;
    }

    public void setNombreEquipo(String nombreEquipo) {
        this.nombreEquipo = nombreEquipo;
    }

    public String getNombreEquipoCorto() {
        return nombreEquipoCorto;
    }

    public void setNombreEquipoCorto(String nombreEquipoCorto) {
        this.nombreEquipoCorto = nombreEquipoCorto;
    }

    @XmlTransient
    public Collection<Pilotos> getPilotosCollection() {
        return pilotosCollection;
    }

    public void setPilotosCollection(Collection<Pilotos> pilotosCollection) {
        this.pilotosCollection = pilotosCollection;
    }

    public Categorias getCategoria() {
        return categoria;
    }

    public void setCategoria(Categorias categoria) {
        this.categoria = categoria;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Escuderias)) {
            return false;
        }
        Escuderias other = (Escuderias) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Escuderias[ id=" + id + " ]";
    }
    
}
