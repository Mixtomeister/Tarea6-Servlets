window.addEventListener('load', callbackLoad);
var categorias;

function callbackLoad(){
    var serv = new ServletManager('Categorias');
    serv.getAll((res) => {
        categorias = res;
        var sel = document.getElementById('selectCategoria');
        categorias.forEach(i => {
            sel.innerHTML += "<option value=\"" + i.id + "\">" + i.categoria + "</option>";
        });
    });
}

function del(){
    var serv = new ServletManager('Categorias');
    serv.del(document.getElementById('selectCategoria').value, delDone);
}

function delDone(res){
    if(res.estado == "OK"){
        alert("Categoria eliminada correctamente");
    }else{
        alert("Error al eliminar Categoria");
    }
}