window.addEventListener('load', callbackLoad);
var categoria, escuderias, act;

function callbackLoad(){
    var servCategoria = new ServletManager('Categorias');
    var servEscuderias = new ServletManager('Escuderias');
    servCategoria.getAll((res) => {
        categoria = res;
        var sel = document.getElementById('selectCategorias');
        categoria.forEach(i => {
            sel.innerHTML += "<option value=\"" + i.id + "\">" + i.categoria + "</option>";
        });
    });
    servEscuderias.getAll((res) => {
        escuderias = res;
        var sel = document.getElementById('selectEscuderia');
        escuderias.forEach(i => {
            sel.innerHTML += "<option value=\"" + i.id + "\">" + i.nombreEquipoCorto + "</option>";
        });
    });
}

function change(){
    act = document.getElementById('selectEscuderia').value;
    escuderias.forEach(i => {
        if(i.id == act){
            document.getElementById('nombre').value = i.nombreEquipo;
            document.getElementById('nombreCorto').value = i.nombreEquipoCorto;
            document.getElementById('selectEscuderias').value = i.escuderia.id;
        }
    });
}

function update(){
    var serv = new ServletManager('Escuderias');
    var esc, id;
    categoria.forEach(i => {
        if(i.id == document.getElementById('selectCategorias').value){
            esc = i;
        }
    });
    var piloto = {
        nombreEquipo: document.getElementById('nombre').value,
        nombreEquipoCorto: document.getElementById('nombreCorto').value,
        categoria: esc
    }
    serv.update(piloto, act, updateDone);
}

function updateDone(res){
    if(res.estado == "OK"){
        alert("Piloto modificado correctamente");
    }else{
        alert("Error al modificar piloto");
    }
}