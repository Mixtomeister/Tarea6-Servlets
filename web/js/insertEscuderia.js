window.addEventListener('load', callbackLoad);
var categorias;

function callbackLoad(){
    var serv = new ServletManager('Categorias');
    serv.getAll((res) => {
        categorias = res;
        categorias.forEach(i => {
            document.getElementById('selectCategorias').innerHTML += "<option value=\"" + i.id + "\">" + i.categoria + "</option>";
        });
        document.getElementById('selectCategorias').setAttribute("required", "true");
    });
}

function insert(){
    var serv = new ServletManager('Escuderias');
    var esc;
    categorias.forEach(i => {
        if(i.id == document.getElementById('selectCategorias').value){
            esc = i;
        }
    });
    var piloto = {
        nombreEquipo: document.getElementById('nombre').value,
        nombreEquipoCorto: document.getElementById('nombreCorto').value,
        categoria: esc
    }
    serv.insert(piloto, insertDone);
}

function insertDone(res){
    if(res.estado == "OK"){
        alert("Escuderia insertada correctamente");
    }else{
        alert("Error al insertar escuderia");
    }
}