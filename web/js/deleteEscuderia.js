window.addEventListener('load', callbackLoad);
var escuderias;

function callbackLoad(){
    var serv = new ServletManager('Escuderias');
    serv.getAll((res) => {
        escuderias = res;
        var sel = document.getElementById('selectEscuderia');
        escuderias.forEach(i => {
            sel.innerHTML += "<option value=\"" + i.id + "\">" + i.nombreEquipoCorto + "</option>";
        });
    });
}

function del(){
    var serv = new ServletManager('Escuderias');
    serv.del(document.getElementById('selectEscuderia').value, delDone);
}

function delDone(res){
    if(res.estado == "OK"){
        alert("Escuderia eliminada correctamente");
    }else{
        alert("Error al eliminar escuderia");
    }
}