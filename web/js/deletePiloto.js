window.addEventListener('load', callbackLoad);
var pilotos;

function callbackLoad(){
    var serv = new ServletManager('Pilotos');
    serv.getAll((res) => {
        pilotos = res;
        var sel = document.getElementById('selectPilotos');
        pilotos.forEach(i => {
            sel.innerHTML += "<option value=\"" + i.id + "\">" + i.nombre + " " + i.apellido + "</option>";
        });
    });
}

function del(){
    var serv = new ServletManager('Pilotos');
    serv.del(document.getElementById('selectPilotos').value, delDone);
}

function delDone(res){
    if(res.estado == "OK"){
        alert("Piloto eliminado correctamente");
    }else{
        alert("Error al eliminar piloto");
    }
}