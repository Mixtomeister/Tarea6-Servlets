window.addEventListener('load', callbackLoad);
var categorias;

function callbackLoad(){
    var serv = new ServletManager('Categorias');
    serv.getAll((res) => {
        categorias = res;
        var tabla = document.getElementById('tabla');
        categorias.forEach(i => {
            tabla.innerHTML += "<tr><td>" + i.categoria + "</td></tr>"; 
        });
    });
}