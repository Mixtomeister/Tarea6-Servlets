window.addEventListener('load', callbackLoad);
var pilotos;

function callbackLoad(){
    var serv = new ServletManager('Pilotos');
    serv.getAll((res) => {
        pilotos = res;
        var tabla = document.getElementById('tabla');
        pilotos.forEach(i => {
            if(i.escuderia == undefined){
                tabla.innerHTML += "<tr><td>" + i.nombre + "</td><td>" + i.apellido + "</td><td>-</td><td>-</td></tr>";    
            }else{
                tabla.innerHTML += "<tr><td>" + i.nombre + "</td><td>" + i.apellido + "</td><td>" + i.escuderia.nombreEquipoCorto + "</td><td>" + i.escuderia.categoria.categoria + "</td></tr>";
            }
        });
    });
}