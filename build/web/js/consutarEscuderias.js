window.addEventListener('load', callbackLoad);
var escuderias;

function callbackLoad(){
    var serv = new ServletManager('Escuderias');
    serv.getAll((res) => {
        escuderias = res;
        var tabla = document.getElementById('tabla');
        escuderias.forEach(i => {
            tabla.innerHTML += "<tr><td>" + i.nombreEquipo + "</td><td>" + i.nombreEquipoCorto + "</td><td>" + i.categoria.categoria + "</td></tr>"; 
        });
    });
}