window.addEventListener('load', callbackLoad);
var escuderias;

function callbackLoad(){
    var serv = new ServletManager('Escuderias');
    serv.getAll((res) => {
        escuderias = res;
        document.getElementById('selectEscuderias').innerHTML += "<option value=\"-\">-</option>";
        escuderias.forEach(i => {
            document.getElementById('selectEscuderias').innerHTML += "<option value=\"" + i.id + "\">" + i.nombreEquipoCorto + "</option>";
        });
        document.getElementById('selectEscuderias').setAttribute("required", "true");
    });
}

function insert(){
    var serv = new ServletManager('Pilotos');
    var esc;
    escuderias.forEach(i => {
        if(i.id == document.getElementById('selectEscuderias').value){
            esc = i;
        }
    });
    var piloto = {
        nombre: document.getElementById('nombre').value,
        apellido: document.getElementById('apellido').value,
        escuderia: esc
    }
    serv.insert(piloto, insertDone);
}

function insertDone(res){
    if(res.estado == "OK"){
        alert("Piloto insertado correctamente");
    }else{
        alert("Error al insertar piloto");
    }
}