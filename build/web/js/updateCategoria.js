window.addEventListener('load', callbackLoad);
var categoria, act;

function callbackLoad(){
    var servPilotos = new ServletManager('Categorias');
    servPilotos.getAll((res) => {
        pilotos = res;
        var sel = document.getElementById('selectCategoria');
        pilotos.forEach(i => {
            sel.innerHTML += "<option value=\"" + i.id + "\">" + i.categoria + "</option>";
        });
    });
}

function change(){
    act = document.getElementById('selectCategoria').value;
    pilotos.forEach(i => {
        if(i.id == act){
            document.getElementById('nombre').value = i.categoria;
        }
    });
}

function update(){
    var serv = new ServletManager('Categorias');
    var piloto = {
        categoria: document.getElementById('nombre').value,
    }
    serv.update(piloto, act, updateDone);
}

function updateDone(res){
    if(res.estado == "OK"){
        alert("Categoria eliminada correctamente");
    }else{
        alert("Error al eliminar Categoria");
    }
}