class ServletManager{
    constructor(serv){
        this.servlet = serv;
    }
    
    getAll(callback){
        $.ajax({
            url: "GestionServlet",
            method: "POST",
            data: {
                pet: "getAll",
                table: this.servlet
            }
        }).done(function(res){
            callback(JSON.parse(res));
        }).fail(function(){
            console.log("get fail");
        });
    }

    insert(obj, callback){
        $.ajax({
            url: "GestionServlet",
            method: "POST",
            data: {
                pet: "insert",
                table: this.servlet,
                item: JSON.stringify(obj)
            }
        }).done(function(res){
            callback(JSON.parse(res));
        }).fail(function(){
            console.log("insert fail");
        });
    }

    del(id, callback){
        $.ajax({
            url: "GestionServlet",
            method: "POST",
            data: {
                pet: "delete",
                table: this.servlet,
                item: id
            }
        }).done(function(res){
            callback(JSON.parse(res));
        }).fail(function(){
            console.log("delete fail");
        });
    }

    update(obj, id, callback){
        $.ajax({
            url: "GestionServlet",
            method: "POST",
            data: {
                pet: "update",
                table: this.servlet,
                item: JSON.stringify(obj),
                id: id
            }
        }).done(function(res){
            callback(JSON.parse(res));
        }).fail(function(){
            console.log("update fail");
        });
    }
}