window.addEventListener('load', callbackLoad);
var pilotos, escuderias, act;

function callbackLoad(){
    var servPilotos = new ServletManager('Pilotos');
    var servEscuderias = new ServletManager('Escuderias');
    servPilotos.getAll((res) => {
        pilotos = res;
        var sel = document.getElementById('selectPilotos');
        pilotos.forEach(i => {
            sel.innerHTML += "<option value=\"" + i.id + "\">" + i.nombre + " " + i.apellido + "</option>";
        });
    });
    servEscuderias.getAll((res) => {
        escuderias = res;
        var sel = document.getElementById('selectEscuderias');
        sel.innerHTML += "<option value=\"-\">-</option>";
        escuderias.forEach(i => {
            sel.innerHTML += "<option value=\"" + i.id + "\">" + i.nombreEquipoCorto + "</option>";
        });
    });
}

function change(){
    act = document.getElementById('selectPilotos').value;
    pilotos.forEach(i => {
        if(i.id == act){
            document.getElementById('nombre').value = i.nombre;
            document.getElementById('apellido').value = i.apellido;
            if(i.escuderia == undefined){
                document.getElementById('selectEscuderias').value = "-";
            }else{
                document.getElementById('selectEscuderias').value = i.escuderia.id;
            }
            
        }
    });
}

function update(){
    var serv = new ServletManager('Pilotos');
    var esc, id;
    escuderias.forEach(i => {
        if(i.id == document.getElementById('selectEscuderias').value){
            esc = i;
        }
    });
    pilotos.forEach(i => {
        if(i.id == act){
            id = i.id;
        }
    });
    var piloto = {
        nombre: document.getElementById('nombre').value,
        apellido: document.getElementById('apellido').value,
        escuderia: esc
    }
    serv.update(piloto, id, updateDone);
}

function updateDone(res){
    if(res.estado == "OK"){
        alert("Piloto modificado correctamente");
    }else{
        alert("Error al modificar piloto");
    }
}