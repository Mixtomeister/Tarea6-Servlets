package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import model.Categorias;
import model.Pilotos;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-12-06T00:01:21")
@StaticMetamodel(Escuderias.class)
public class Escuderias_ { 

    public static volatile SingularAttribute<Escuderias, String> nombreEquipoCorto;
    public static volatile CollectionAttribute<Escuderias, Pilotos> pilotosCollection;
    public static volatile SingularAttribute<Escuderias, String> nombreEquipo;
    public static volatile SingularAttribute<Escuderias, Categorias> categoria;
    public static volatile SingularAttribute<Escuderias, Integer> id;

}