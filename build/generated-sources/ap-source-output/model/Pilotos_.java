package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import model.Escuderias;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-12-06T00:01:21")
@StaticMetamodel(Pilotos.class)
public class Pilotos_ { 

    public static volatile SingularAttribute<Pilotos, String> apellido;
    public static volatile SingularAttribute<Pilotos, Integer> id;
    public static volatile SingularAttribute<Pilotos, String> nombre;
    public static volatile SingularAttribute<Pilotos, Escuderias> escuderia;

}