package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import model.Escuderias;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-12-06T00:01:21")
@StaticMetamodel(Categorias.class)
public class Categorias_ { 

    public static volatile SingularAttribute<Categorias, String> categoria;
    public static volatile CollectionAttribute<Categorias, Escuderias> escuderiasCollection;
    public static volatile SingularAttribute<Categorias, Integer> id;

}