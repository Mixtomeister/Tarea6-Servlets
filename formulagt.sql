-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.26-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.4.0.5185
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para formulagt
CREATE DATABASE IF NOT EXISTS `formulagt` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci */;
USE `formulagt`;

-- Volcando estructura para tabla formulagt.categorias
CREATE TABLE IF NOT EXISTS `categorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoria` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla formulagt.categorias: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `categorias` DISABLE KEYS */;
INSERT INTO `categorias` (`id`, `categoria`) VALUES
	(1, 'Formula 1'),
	(2, 'Formula E'),
	(3, 'Formula 2'),
	(4, 'Formula 3');
/*!40000 ALTER TABLE `categorias` ENABLE KEYS */;

-- Volcando estructura para procedimiento formulagt.deleteAllEscuderias
DELIMITER //
CREATE DEFINER=`sa`@`localhost` PROCEDURE `deleteAllEscuderias`()
BEGIN

DELETE FROM escuderias;

END//
DELIMITER ;

-- Volcando estructura para procedimiento formulagt.deleteEscuderia
DELIMITER //
CREATE DEFINER=`sa`@`localhost` PROCEDURE `deleteEscuderia`(
	IN `p_id` INT
)
BEGIN

DELETE FROM escuderias
WHERE id = p_id;

END//
DELIMITER ;

-- Volcando estructura para tabla formulagt.escuderias
CREATE TABLE IF NOT EXISTS `escuderias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_equipo` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `nombre_equipo_corto` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `categoria` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `escuderia-categoria` (`categoria`),
  CONSTRAINT `escuderia-categoria` FOREIGN KEY (`categoria`) REFERENCES `categorias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla formulagt.escuderias: ~10 rows (aproximadamente)
/*!40000 ALTER TABLE `escuderias` DISABLE KEYS */;
INSERT INTO `escuderias` (`id`, `nombre_equipo`, `nombre_equipo_corto`, `categoria`) VALUES
	(4, 'Mercedes AMG Petronas Motorsport', 'Mercedes', 1),
	(5, 'Scuderia Ferrari', 'Ferrari', 1),
	(9, 'Red Bull Racing', 'Red Bull Racing', 1),
	(10, 'Haas F1 Team', 'Haas', 1),
	(11, 'Renault Sport Formula One Team', 'Renault', 1),
	(12, 'McLaren Honda Formula 1 Team', 'McLaren Honda', 1),
	(13, 'Sahara Force India F1 Team', 'Force India', 1),
	(14, 'Sauber F1 Team', 'Sauber', 1),
	(15, 'Scuderia Toro Rosso', 'Toro Rosso', 1),
	(16, 'Williams Martini Racing', 'Williams', 1);
/*!40000 ALTER TABLE `escuderias` ENABLE KEYS */;

-- Volcando estructura para procedimiento formulagt.getCategorias
DELIMITER //
CREATE DEFINER=`sa`@`localhost` PROCEDURE `getCategorias`()
BEGIN

SELECT cat.id, cat.categoria
FROM categorias AS cat;

END//
DELIMITER ;

-- Volcando estructura para procedimiento formulagt.getEscuderias
DELIMITER //
CREATE DEFINER=`sa`@`localhost` PROCEDURE `getEscuderias`()
BEGIN

SELECT es.id, es.nombre_equipo, es.nombre_equipo_corto, es.categoria
FROM escuderias as es;

END//
DELIMITER ;

-- Volcando estructura para procedimiento formulagt.getTablas
DELIMITER //
CREATE DEFINER=`sa`@`localhost` PROCEDURE `getTablas`()
BEGIN

SHOW FULL TABLES FROM formulagt;

END//
DELIMITER ;

-- Volcando estructura para procedimiento formulagt.insertEscuderia
DELIMITER //
CREATE DEFINER=`sa`@`localhost` PROCEDURE `insertEscuderia`(
	IN `p_ne` VARCHAR(200),
	IN `p_nec` VARCHAR(50),
	IN `p_cat` INT
)
BEGIN

INSERT INTO escuderias(nombre_equipo, nombre_equipo_corto, categoria)
VALUES (p_ne, p_nec, p_cat);

END//
DELIMITER ;

-- Volcando estructura para procedimiento formulagt.likeEscuderia
DELIMITER //
CREATE DEFINER=`sa`@`localhost` PROCEDURE `likeEscuderia`(
	IN `p_ne` VARCHAR(200)

)
BEGIN

SELECT es.id, es.nombre_equipo, es.nombre_equipo_corto, es.categoria
FROM escuderias AS es
WHERE es.nombre_equipo LIKE CONCAT('%', p_ne, '%')
OR es.nombre_equipo_corto LIKE CONCAT('%', p_ne, '%');

END//
DELIMITER ;

-- Volcando estructura para tabla formulagt.pilotos
CREATE TABLE IF NOT EXISTS `pilotos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `apellido` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `escuderia` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `escuderia` (`escuderia`),
  CONSTRAINT `fk_escuderia_piloto` FOREIGN KEY (`escuderia`) REFERENCES `escuderias` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla formulagt.pilotos: ~23 rows (aproximadamente)
/*!40000 ALTER TABLE `pilotos` DISABLE KEYS */;
INSERT INTO `pilotos` (`id`, `nombre`, `apellido`, `escuderia`) VALUES
	(1, 'Lewis', 'Hamilton', 4),
	(2, 'Valtteri', 'Bottas', 4),
	(3, 'Sebastian', 'Vettel', 5),
	(4, 'Kimi', 'Raikkonen', 5),
	(5, 'Romain', 'Grosjean', 10),
	(6, 'Kevin', 'Magnussen', 10),
	(7, 'Nicolas', 'Hulkenberg', 11),
	(8, 'Jolyon', 'Palmer', NULL),
	(9, 'Fernando', 'Alonso', 12),
	(10, 'Stoffel', 'Vandoorne', 12),
	(11, 'Daniel', 'Ricciardo', 9),
	(12, 'Max', 'Verstappen', 9),
	(13, 'Sergio', 'Perez', 13),
	(14, 'Esteban', 'Ocon', 13),
	(15, 'Marcus', 'Ericsson', 14),
	(16, 'Pascal', 'Wehrlein', 14),
	(17, 'Daniil', 'Kvyat', NULL),
	(18, 'Carlos', 'Sainz', 11),
	(19, 'Felipe', 'Massa', 16),
	(20, 'Lance', 'Stroll', 14),
	(21, 'Roberto', 'Merhi', NULL),
	(22, 'Pierre', 'Gastly', 15),
	(23, 'Charles', 'Leclerc', 14);
/*!40000 ALTER TABLE `pilotos` ENABLE KEYS */;

-- Volcando estructura para procedimiento formulagt.updateEscuderia
DELIMITER //
CREATE DEFINER=`sa`@`localhost` PROCEDURE `updateEscuderia`(
	IN `p_id` INT,
	IN `p_ne` VARCHAR(200),
	IN `p_nec` VARCHAR(100),
	IN `p_cat` INT
)
BEGIN

UPDATE escuderias AS es
SET es.nombre_equipo = p_ne,
es.nombre_equipo_corto = p_nec,
es.categoria = p_cat
WHERE es.id = p_id;

END//
DELIMITER ;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
